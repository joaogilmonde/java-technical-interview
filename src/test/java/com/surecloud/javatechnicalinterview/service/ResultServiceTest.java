package com.surecloud.javatechnicalinterview.service;

import com.surecloud.javatechnicalinterview.entity.Result;
import com.surecloud.javatechnicalinterview.exception.ResultNotFoundException;
import com.surecloud.javatechnicalinterview.model.ResultDto;
import com.surecloud.javatechnicalinterview.repository.ResultRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ResultServiceTest {

    @Mock
    ResultRepository resultRepository;

    private ResultService resultService;

    @BeforeAll
    public void init() {
        MockitoAnnotations.openMocks(this);
        resultService = new ResultService(resultRepository);
    }

    @Test
    void testGetResultListHappyPath() {
        List<Result> results = new ArrayList<>();
        UUID id = UUID.randomUUID();
        Result result = new Result().setId(id).setName("test").setScore(1).setDate_taken(new Date());
        results.add(result);

        when(resultRepository.findAll()).thenReturn(results);

        List<ResultDto> resultDtos = resultService.getResultList();
        assertFalse(resultDtos.isEmpty());
        assertEquals(resultDtos.get(0).id(), result.getId().toString());
    }

    @Test
    void testGetResultListEmpty() {
        List<Result> results = new ArrayList<>();
        when(resultRepository.findAll()).thenReturn(results);

        List<ResultDto> resultDtos = resultService.getResultList();
        assertTrue(resultDtos.isEmpty());
    }

    @Test
    void testGetResultHappyPath() {
        UUID id = UUID.randomUUID();
        Result result = new Result().setId(id).setName("test").setScore(1).setDate_taken(new Date());
        when(resultRepository.findById(id)).thenReturn(Optional.of(result));

        ResultDto resultDto = resultService.getResult(id.toString());
        assertEquals(resultDto.id(), result.getId().toString());
    }

    @Test
    void testGetResultNotFoundWrongId() {
        String id = "wrongId";
        assertThrows(ResultNotFoundException.class, () -> resultService.getResult(id));
    }

    @Test
    void testGetResultNotFoundException() {
        UUID id = UUID.randomUUID();
        when(resultRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResultNotFoundException.class, () -> resultService.getResult(id.toString()));
    }

    @Test
    void testCreateResultHappyPath() throws ParseException {
        ResultDto resultDto = new ResultDto(null, "name", 1, "2022-01-21");
        Result savedResult = new Result()
                .setId(UUID.randomUUID())
                .setName("name")
                .setScore(1)
                .setDate_taken(new SimpleDateFormat("dd-MM-yyyy").parse("2022-01-21"));
        when(resultRepository.save(any())).thenReturn(savedResult);

        ResultDto resultDtoCreated = resultService.createResult(resultDto);
        assertEquals(resultDtoCreated.id(), savedResult.getId().toString());
        assertEquals(resultDtoCreated.name(), savedResult.getName());
        assertEquals(resultDtoCreated.score(), savedResult.getScore());
    }

    @Test
    void testCreateResultWrongDataExpression(){
        ResultDto resultDto = new ResultDto(null, "name", 1, "2022/01/21");
        assertThrows(ParseException.class, () -> resultService.createResult(resultDto));
    }
}
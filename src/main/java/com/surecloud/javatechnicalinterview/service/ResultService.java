package com.surecloud.javatechnicalinterview.service;

import com.surecloud.javatechnicalinterview.entity.Result;
import com.surecloud.javatechnicalinterview.exception.ResultNotFoundException;
import com.surecloud.javatechnicalinterview.model.ResultDto;
import com.surecloud.javatechnicalinterview.repository.ResultRepository;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ResultService {

    private final ResultRepository resultRepository;

    public ResultService(ResultRepository resultRepository) {
        this.resultRepository = resultRepository;
    }

    public List<ResultDto> getResultList() {
        List<Result> results = (List<Result>) resultRepository.findAll();
        List<ResultDto> resultDtoList = new ArrayList<>();
        for (Result result : results) {
            ResultDto resultDto = getResultDto(result);
            resultDtoList.add(resultDto);
        }

        return resultDtoList;
    }

    public ResultDto getResult(String id) {
        try {
            Optional<Result> optionalResult = resultRepository.findById(UUID.fromString(id));
            Result result = optionalResult.orElseThrow(() ->
                    new ResultNotFoundException("Result with id: " + id + " not found"));
            return getResultDto(result);
        } catch (IllegalArgumentException e) {
            throw new ResultNotFoundException("Result with id: " + id + " not found");
        }
    }

    public ResultDto createResult(ResultDto resultDto) throws ParseException {
        Date date_taken = new SimpleDateFormat("dd-MM-yyyy").parse(resultDto.dateTaken());
        Result result = new Result()
                .setName(resultDto.name())
                .setScore(resultDto.score())
                .setDate_taken(date_taken);
        result = resultRepository.save(result);
        return getResultDto(result);
    }

    private ResultDto getResultDto(Result result) {
        return new ResultDto(result.getId().toString(),
                result.getName(),
                result.getScore(),
                result.getDate_taken().toInstant().toString());
    }
}

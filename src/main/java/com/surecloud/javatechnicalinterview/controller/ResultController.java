package com.surecloud.javatechnicalinterview.controller;

import com.surecloud.javatechnicalinterview.model.ResultDto;
import com.surecloud.javatechnicalinterview.service.ResultService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RequestMapping("/result")
@RestController
public class ResultController {

    private final ResultService resultService;

    public ResultController(ResultService resultService) {
        this.resultService = resultService;
    }

    @GetMapping
    private ResponseEntity<List<ResultDto>> getResultList() {
        return ResponseEntity.ok(resultService.getResultList());
    }

    @GetMapping("/{id}")
    private ResponseEntity<ResultDto> getResult(@PathVariable String id) {
        return ResponseEntity.ok(resultService.getResult(id));
    }

    @PostMapping
    private ResponseEntity<ResultDto> createResult(@RequestBody ResultDto resultDto) throws ParseException {
        return new ResponseEntity<ResultDto>(resultService.createResult(resultDto), HttpStatus.CREATED);
    }

}

package com.surecloud.javatechnicalinterview.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.text.ParseException;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ResultNotFoundException.class)
    protected ResponseEntity<String> handleResultNotFoundException(ResultNotFoundException exception) {
        return new ResponseEntity<String>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ParseException.class)
    protected ResponseEntity<String> handleParseException(ParseException exception) {
        return new ResponseEntity<String>("Unparseable date use the format yyyy-MM-dd", HttpStatus.BAD_REQUEST);
    }
}

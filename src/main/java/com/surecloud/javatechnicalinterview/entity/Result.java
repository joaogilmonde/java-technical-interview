package com.surecloud.javatechnicalinterview.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.UUID;


@Entity
public class Result {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    private String name;

    private int score;

    private Date date_taken;



    public UUID getId() {
        return id;
    }

    public Result setId(UUID id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Result setName(String name) {
        this.name = name;
        return this;
    }

    public int getScore() {
        return score;
    }

    public Result setScore(int score) {
        this.score = score;
        return this;
    }

    public Date getDate_taken() {
        return date_taken;
    }

    public Result setDate_taken(Date date_taken) {
        this.date_taken = date_taken;
        return this;
    }
}

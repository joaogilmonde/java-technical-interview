package com.surecloud.javatechnicalinterview.model;


import org.springframework.lang.Nullable;

public record ResultDto(@Nullable String id, String name, int score, String dateTaken) {
}

package com.surecloud.javatechnicalinterview.repository;

import com.surecloud.javatechnicalinterview.entity.Result;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ResultRepository extends CrudRepository<Result, UUID> {
}
